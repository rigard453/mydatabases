-- MySQL dump 10.13  Distrib 8.0.28, for macos12.2 (x86_64)
--
-- Host: localhost    Database: Rockefellers
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `family`
--

DROP TABLE IF EXISTS `family`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `family` (
  `family_id` int NOT NULL,
  `family_name` char(50) DEFAULT NULL,
  PRIMARY KEY (`family_id`),
  UNIQUE KEY `family_family_id_uindex` (`family_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Знаменитые семьи';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `family`
--

LOCK TABLES `family` WRITE;
/*!40000 ALTER TABLE `family` DISABLE KEYS */;
INSERT INTO `family` VALUES (325,'Гейтс'),(326,'Джобс'),(390,'Рокфеллер'),(583,'Гетти'),(1433,'Кеннеди'),(1434,'Вандербильт'),(1435,'Дюпон');
/*!40000 ALTER TABLE `family` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `firm_owners`
--

DROP TABLE IF EXISTS `firm_owners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `firm_owners` (
  `firm_id` int DEFAULT NULL,
  `owner_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `firm_owners`
--

LOCK TABLES `firm_owners` WRITE;
/*!40000 ALTER TABLE `firm_owners` DISABLE KEYS */;
INSERT INTO `firm_owners` VALUES (404,390),(610,390),(611,390),(612,390),(613,390),(614,390),(615,440),(616,390);
/*!40000 ALTER TABLE `firm_owners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `firms`
--

DROP TABLE IF EXISTS `firms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `firms` (
  `id` int NOT NULL,
  `nameFirm` char(100) DEFAULT NULL,
  `psevdonim` char(100) DEFAULT NULL,
  UNIQUE KEY `firms_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Управляемые фирмы и корпорации';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `firms`
--

LOCK TABLES `firms` WRITE;
/*!40000 ALTER TABLE `firms` DISABLE KEYS */;
INSERT INTO `firms` VALUES (404,'Standard Oil',NULL),(610,'Standard Oil Trust','Standard Oil'),(611,'ConocoPhillips',NULL),(612,'BP',NULL),(613,'Chevron',NULL),(614,'ExxonMobil',NULL),(615,'Paravel',NULL),(616,'Chase','Chase Bank');
/*!40000 ALTER TABLE `firms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `husbands_and_wives`
--

DROP TABLE IF EXISTS `husbands_and_wives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `husbands_and_wives` (
  `husband` int DEFAULT NULL,
  `wives` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Связь между мужьями и женами через их ID';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `husbands_and_wives`
--

LOCK TABLES `husbands_and_wives` WRITE;
/*!40000 ALTER TABLE `husbands_and_wives` DISABLE KEYS */;
INSERT INTO `husbands_and_wives` VALUES (384,366),(384,368),(384,379),(439,440),(331,443),(476,1553);
/*!40000 ALTER TABLE `husbands_and_wives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interacting_people`
--

DROP TABLE IF EXISTS `interacting_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `interacting_people` (
  `people_id` int DEFAULT NULL,
  `people_name` char(100) DEFAULT NULL,
  `psevdonim` char(100) DEFAULT NULL,
  `profession` char(100) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  UNIQUE KEY `interacting_people_people_id_uindex` (`people_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Люди, которые взаимодействуют с рокфеллерами в каком либо виде';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interacting_people`
--

LOCK TABLES `interacting_people` WRITE;
/*!40000 ALTER TABLE `interacting_people` DISABLE KEYS */;
INSERT INTO `interacting_people` VALUES (459,'Айви Ли',NULL,'пиар агент','m'),(483,'Рузвельт','Теодор Рузвельт','президент США','m'),(484,'Эйзенхауэр',NULL,'президент США','m'),(486,'Джеральд Форд',NULL,'президент США','m'),(526,'Михаил Горбачев',NULL,'президент СССР','m'),(527,'Фидель Кастро',NULL,'президент Кубы','m'),(532,'Генри Киссинджер','Киссинджер','советник президента США','m'),(535,'Аллен Даллес',NULL,'директор ЦРУ','m'),(728,'Генри Флэглер',NULL,'партнер Standard Oil','m'),(732,'Джабез Боствик',NULL,'партнер Standard Oil','m'),(799,'Эдвин Дрейк',NULL,'ученый','m'),(841,'Ричард Бэрд',NULL,'ученый','m'),(847,'Билл Гейтс',NULL,'президент Майкрософт','m'),(1146,'Голда Меир',NULL,'президент Израиля','f'),(1158,'Чжоу Эньлай',NULL,'глава КНР','m'),(1161,'Анвар Садат',NULL,'президент Египта','m'),(1164,'Леонид Брежнев',NULL,'президент СССР','m'),(1671,'Стефан Харкенс',NULL,'партнер Standard Oil','m');
/*!40000 ALTER TABLE `interacting_people` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paramour`
--

DROP TABLE IF EXISTS `paramour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paramour` (
  `paramour_id` int DEFAULT NULL,
  `paramour_name` char(50) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  UNIQUE KEY `paramour_paramour_id_uindex` (`paramour_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Любовники и любовнцы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paramour`
--

LOCK TABLES `paramour` WRITE;
/*!40000 ALTER TABLE `paramour` DISABLE KEYS */;
INSERT INTO `paramour` VALUES (512,'Меган Маршак','f');
/*!40000 ALTER TABLE `paramour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `public_organizations`
--

DROP TABLE IF EXISTS `public_organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `public_organizations` (
  `id` int NOT NULL,
  `name_PO` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `public_organizations_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `public_organizations`
--

LOCK TABLES `public_organizations` WRITE;
/*!40000 ALTER TABLE `public_organizations` DISABLE KEYS */;
INSERT INTO `public_organizations` VALUES (348,'Бильдербергский клуб'),(426,'Рокфеллеровский университет'),(465,'Фонд Рокфеллеров'),(467,'Рокфеллер-центр'),(560,'Университет Чикаго'),(579,'Метрополитен-опера'),(580,'Линкольн-центр');
/*!40000 ALTER TABLE `public_organizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rockefellers_InteractingPeople`
--

DROP TABLE IF EXISTS `rockefellers_InteractingPeople`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rockefellers_InteractingPeople` (
  `rockefellers_id` int DEFAULT NULL,
  `interactPeople_id` int DEFAULT NULL,
  `power_interacting` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Рокфеллеры и взаимодействующие люди. Учет силы взаимодействия как частоты совместного упоминания в статьях.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rockefellers_InteractingPeople`
--

LOCK TABLES `rockefellers_InteractingPeople` WRITE;
/*!40000 ALTER TABLE `rockefellers_InteractingPeople` DISABLE KEYS */;
INSERT INTO `rockefellers_InteractingPeople` VALUES (575,483,1),(575,484,1),(476,526,1),(476,527,1),(476,532,1),(575,535,1),(449,728,1),(449,732,1),(449,799,1),(449,841,1),(476,1146,1),(476,1158,1),(476,1161,1),(476,1164,1),(449,1671,1);
/*!40000 ALTER TABLE `rockefellers_InteractingPeople` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rockefellers_man`
--

DROP TABLE IF EXISTS `rockefellers_man`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rockefellers_man` (
  `id` int NOT NULL,
  `name` char(50) DEFAULT NULL,
  `psevdonim` char(50) DEFAULT NULL,
  `psevdonim_2` char(50) DEFAULT NULL,
  `gender` char(2) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `Date_of_death` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rockefellers_man_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rockefellers_man`
--

LOCK TABLES `rockefellers_man` WRITE;
/*!40000 ALTER TABLE `rockefellers_man` DISABLE KEYS */;
INSERT INTO `rockefellers_man` VALUES (331,'Д.Рокфеллер-старший','Джон Рокфеллер-старший',NULL,'m',NULL,NULL),(384,'Билл Рокфеллер','Уильям Рокфеллер-старший',NULL,'m',NULL,NULL),(439,'Джастин Рокфеллер',NULL,NULL,'m',NULL,NULL),(449,'Джон Д. Рокфеллер','Джон Рокфеллер-младший',NULL,'m',NULL,NULL),(474,'Джон-третий Рокфеллер','Джон Рокфеллер-третий',NULL,'m',NULL,NULL),(475,'Лоуренс Рокфеллер',NULL,NULL,'m',NULL,NULL),(476,'Дэвид Рокфеллер',NULL,NULL,'m',NULL,NULL),(479,'Уинтроп Рокфеллер',NULL,NULL,'m',NULL,NULL),(542,'Джей Рокфеллер',NULL,NULL,'m',NULL,NULL),(551,'Майкл Рокфеллер',NULL,NULL,'m',NULL,NULL),(575,'Нельсон Рокфеллер',NULL,NULL,'m',NULL,NULL),(852,'Алиса Рокфеллер',NULL,NULL,'f',NULL,NULL),(853,'Альта Рокфеллер',NULL,NULL,'f',NULL,NULL),(854,'Эдит Рокфеллер',NULL,NULL,'f',NULL,NULL),(855,'Елизабет Рокфеллер',NULL,NULL,'f',NULL,NULL);
/*!40000 ALTER TABLE `rockefellers_man` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rockefellers_paramour`
--

DROP TABLE IF EXISTS `rockefellers_paramour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rockefellers_paramour` (
  `rockefellers_id` int DEFAULT NULL,
  `paramour_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='соответствие любовников';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rockefellers_paramour`
--

LOCK TABLES `rockefellers_paramour` WRITE;
/*!40000 ALTER TABLE `rockefellers_paramour` DISABLE KEYS */;
INSERT INTO `rockefellers_paramour` VALUES (575,512);
/*!40000 ALTER TABLE `rockefellers_paramour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `test` (
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100000 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` VALUES (1),(2),(3),(4),(5),(6),(7),(8);
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wives`
--

DROP TABLE IF EXISTS `wives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wives` (
  `wife_id` int NOT NULL,
  `wife_name` char(50) DEFAULT NULL,
  `psevdonim` char(50) DEFAULT NULL,
  `gender` char(2) DEFAULT NULL,
  UNIQUE KEY `wives_wife_id_uindex` (`wife_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Жены Рокфеллеров';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wives`
--

LOCK TABLES `wives` WRITE;
/*!40000 ALTER TABLE `wives` DISABLE KEYS */;
INSERT INTO `wives` VALUES (366,'Нэнси Браун',NULL,'f'),(368,'Элайза Дэвисон',NULL,'f'),(379,'Маргарет Аллен',NULL,'f'),(440,'Индре Рокфеллер',NULL,'f'),(443,'Лаура Сетти Спелман','Лаура Спелман','f'),(1553,'Пегги Рокфеллер',NULL,'f');
/*!40000 ALTER TABLE `wives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work_table`
--

DROP TABLE IF EXISTS `work_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `work_table` (
  `id` int NOT NULL AUTO_INCREMENT,
  `work_meaning` char(100) DEFAULT NULL COMMENT 'рабочие слова и выражения',
  `psevdomin` char(100) DEFAULT NULL,
  `wife` int DEFAULT NULL,
  `profession` char(100) DEFAULT NULL,
  `public_organization` char(100) DEFAULT NULL,
  `another_family` char(100) DEFAULT NULL,
  `gender` char(2) DEFAULT NULL,
  `child` int DEFAULT NULL,
  `worked_with` int DEFAULT NULL,
  `firms` int DEFAULT NULL,
  `mistress` int DEFAULT NULL,
  `hause` int DEFAULT NULL,
  `owner` int DEFAULT NULL,
  `family_member` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1899 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Промежуточные результаты для анализа';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_table`
--

LOCK TABLES `work_table` WRITE;
/*!40000 ALTER TABLE `work_table` DISABLE KEYS */;
INSERT INTO `work_table` VALUES (728,'Генри Флэглер',NULL,NULL,'партнер Standard Oil',NULL,NULL,'m',NULL,449,NULL,NULL,NULL,NULL,NULL),(732,'Джабез Боствик',NULL,NULL,'партнер Standard Oil',NULL,NULL,'m',NULL,449,NULL,NULL,NULL,NULL,NULL),(799,'Эдвин Дрейк',NULL,NULL,'ученый',NULL,NULL,'m',NULL,449,NULL,NULL,NULL,NULL,NULL),(841,'Ричард Бэрд',NULL,NULL,'ученый',NULL,NULL,'m',NULL,449,NULL,NULL,NULL,NULL,NULL),(847,'Билл Гейтс',NULL,NULL,'президент Майкрософт',NULL,'325','m',NULL,NULL,NULL,NULL,NULL,NULL,325),(852,'Алиса Рокфеллер',NULL,NULL,NULL,NULL,NULL,'f',449,NULL,NULL,NULL,NULL,NULL,1),(853,'Альта Рокфеллер',NULL,NULL,NULL,NULL,NULL,'f',449,NULL,NULL,NULL,NULL,NULL,1),(854,'Эдит Рокфеллер',NULL,NULL,NULL,NULL,NULL,'f',449,NULL,NULL,NULL,NULL,NULL,1),(855,'Елизабет Рокфеллер',NULL,NULL,NULL,NULL,NULL,'f',449,NULL,NULL,NULL,NULL,NULL,1),(1146,'Голда Меир',NULL,NULL,'президент Израиля',NULL,NULL,'m',NULL,476,NULL,NULL,NULL,NULL,NULL),(1158,'Чжоу Эньлай',NULL,NULL,'глава КНР',NULL,NULL,'m',NULL,476,NULL,NULL,NULL,NULL,NULL),(1161,'Анвар Садат',NULL,NULL,'президент Египта',NULL,NULL,'m',NULL,476,NULL,NULL,NULL,NULL,NULL),(1164,'Леонид Брежнев',NULL,NULL,'президент СССР',NULL,NULL,'m',NULL,476,NULL,NULL,NULL,NULL,NULL),(1433,'Кеннеди',NULL,NULL,NULL,NULL,'1433',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1433),(1434,'Вандербильт',NULL,NULL,NULL,NULL,'1434',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1434),(1435,'Дюпон',NULL,NULL,NULL,NULL,'1435',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1435),(1553,'Пегги Рокфеллер',NULL,476,NULL,NULL,NULL,'f',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1671,'Стефан Харкенс',NULL,NULL,'партнер Standard Oil',NULL,NULL,'m',NULL,449,NULL,NULL,NULL,NULL,NULL),(1680,'Покантико-Хиллс',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,390,NULL,390);
/*!40000 ALTER TABLE `work_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-02 21:37:24
