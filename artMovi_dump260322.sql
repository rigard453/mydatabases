-- MySQL dump 10.13  Distrib 8.0.28, for macos12.2 (x86_64)
--
-- Host: localhost    Database: artMovi
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actors`
--

DROP TABLE IF EXISTS `actors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `actors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `names` varchar(100) DEFAULT NULL,
  `country` varchar(50) NOT NULL DEFAULT '',
  `year` int NOT NULL DEFAULT '1970',
  `heatricalcharacter` varchar(50) NOT NULL DEFAULT '',
  `age` int NOT NULL,
  `id_movi` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=279 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actors`
--

LOCK TABLES `actors` WRITE;
/*!40000 ALTER TABLE `actors` DISABLE KEYS */;
INSERT INTO `actors` VALUES (1,'Eva Longoria','FR',1976,'',46,6),(3,'Merfi','FR',1969,'',53,8),(4,'Uilis','FR',1957,'',65,5),(8,'Zalman','FR',1970,'',52,1),(30,'Tom Crus','FR',1968,'Drama',54,5),(36,'Rridli','Fra',2008,'Comedy',14,7),(45,'Shwarceneger','Aus',2002,'Drama',20,7),(46,'Eva Longoria','FR',1970,'',52,14),(47,'Redclif','FR',1970,'',52,5),(48,'Merfi','FR',1970,'',52,11),(49,'Uilis','FR',1970,'',52,11),(50,'Tom Crus','FR',1970,'',52,6),(51,'Rridli','FR',1970,'',52,12),(52,'Shwarceneger','FR',1970,'',52,11),(53,'Dueyn','Yamayka',1981,'',41,6),(54,'Grant Keri','UK',1904,'',118,11),(55,'Garbo Greta','GERM',1905,'',117,6),(56,'Grant Ki','RU',1978,'',44,14),(57,'Бенедикт Самюель','FR',1980,'',42,7),(58,'Eva Longoria','FR',1970,'',46,8),(60,'Merfi','FR',1970,'',53,5),(61,'Uilis','FR',1970,'',65,1),(62,'Zalman','FR',1970,'',52,3),(63,'Tom Crus','FR',1970,'',54,12),(64,'Rridli','USA',1970,'',14,9),(65,'Shwarceneger','USA',1970,'',20,6),(66,'Eva Longoria','FR',1970,'',52,3),(67,'Redclif','FR',1970,'',52,14),(68,'Merfi','FR',1970,'',52,15),(69,'Uilis','FR',1970,'',52,2),(70,'Tom Crus','FR',1970,'',52,10),(71,'Rridli','FR',1970,'',52,13),(72,'Shwarceneger','FR',1970,'',52,6),(73,'Dueyn','FR',1970,'',41,5),(74,'Grant Keri','FR',1970,'',118,9),(75,'Garbo Greta','FR',1970,'',117,1),(76,'Grant Ki','FR',1970,'',44,5),(77,'Бенедикт Самюель','FR',1970,'',42,8),(89,'Eva Longoria','FR',1970,'',46,10),(91,'Merfi','FR',1970,'',53,9),(92,'Uilis','FR',1970,'',65,3),(93,'Zalman','FR',1970,'',52,3),(94,'Tom Crus','FR',1970,'',54,6),(95,'Rridli','USA',1970,'',14,7),(96,'Shwarceneger','USA',1970,'',20,0),(97,'Eva Longoria','FR',1970,'',52,11),(98,'Redclif','FR',1970,'',52,8),(99,'Merfi','FR',1970,'',52,8),(100,'Uilis','FR',1970,'',52,3),(101,'Tom Crus','FR',1970,'',52,4),(102,'Rridli','FR',1970,'',52,11),(103,'Shwarceneger','FR',1970,'',52,14),(104,'Dueyn','FR',1970,'',41,7),(105,'Grant Keri','FR',1970,'',118,9),(106,'Garbo Greta','FR',1970,'',117,7),(107,'Grant Ki','FR',1970,'',44,10),(108,'Бенедикт Самюель','FR',1970,'',42,11),(109,'Eva Longoria','FR',1970,'',46,13),(111,'Merfi','FR',1970,'',53,14),(112,'Uilis','FR',1970,'',65,3),(113,'Zalman','FR',1970,'',52,4),(114,'Tom Crus','FR',1970,'',54,8),(115,'Rridli','USA',1970,'',14,0),(116,'Shwarceneger','USA',1970,'',20,6),(117,'Eva Longoria','FR',1970,'',52,13),(118,'Redclif','FR',1970,'',52,5),(119,'Merfi','FR',1970,'',52,1),(120,'Uilis','FR',1970,'',52,4),(121,'Tom Crus','FR',1970,'',52,4),(122,'Rridli','FR',1970,'',52,5),(123,'Shwarceneger','FR',1970,'',52,1),(124,'Dueyn','FR',1970,'',41,3),(125,'Grant Keri','FR',1970,'',118,14),(126,'Garbo Greta','FR',1970,'',117,2),(127,'Grant Ki','FR',1970,'',44,11),(128,'Бенедикт Самюель','FR',1970,'',42,3),(152,'Eva Longoria','FR',1970,'',46,14),(154,'Merfi','FR',1970,'',53,1),(155,'Uilis','FR',1970,'',65,7),(156,'Zalman','FR',1970,'',52,2),(157,'Tom Crus','FR',1970,'',54,6),(158,'Rridli','USA',1970,'',14,8),(159,'Shwarceneger','USA',1970,'',20,5),(160,'Eva Longoria','FR',1970,'',52,4),(161,'Redclif','FR',1970,'',52,6),(162,'Merfi','FR',1970,'',52,15),(163,'Uilis','FR',1970,'',52,12),(164,'Tom Crus','FR',1970,'',52,14),(165,'Rridli','FR',1970,'',52,7),(166,'Shwarceneger','FR',1970,'',52,5),(167,'Dueyn','FR',1970,'',41,5),(168,'Grant Keri','FR',1970,'',118,10),(169,'Garbo Greta','FR',1970,'',117,3),(170,'Grant Ki','FR',1970,'',44,2),(171,'Бенедикт Самюель','FR',1970,'',42,15),(172,'Eva Longoria','FR',1970,'',46,7),(174,'Merfi','FR',1970,'',53,8),(175,'Uilis','FR',1970,'',65,4),(176,'Zalman','FR',1970,'',52,10),(177,'Tom Crus','FR',1970,'',54,9),(178,'Rridli','USA',1970,'',14,14),(179,'Shwarceneger','USA',1970,'',20,0),(180,'Eva Longoria','FR',1970,'',52,2),(181,'Redclif','FR',1970,'',52,11),(182,'Merfi','FR',1970,'',52,2),(183,'Uilis','FR',1970,'',52,7),(184,'Tom Crus','FR',1970,'',52,13),(185,'Rridli','FR',1970,'',52,14),(186,'Shwarceneger','FR',1970,'',52,3),(187,'Dueyn','FR',1970,'',41,1),(188,'Grant Keri','FR',1970,'',118,10),(189,'Garbo Greta','FR',1970,'',117,5),(190,'Grant Ki','FR',1970,'',44,7),(191,'Бенедикт Самюель','FR',1970,'',42,5),(192,'Eva Longoria','FR',1970,'',46,6),(194,'Merfi','FR',1970,'',53,14),(195,'Uilis','FR',1970,'',65,9),(196,'Zalman','FR',1970,'',52,15),(197,'Tom Crus','FR',1970,'',54,4),(198,'Rridli','USA',1970,'',14,5),(199,'Shwarceneger','USA',1970,'',20,13),(200,'Eva Longoria','FR',1970,'',52,5),(201,'Redclif','FR',1970,'',52,3),(202,'Merfi','FR',1970,'',52,12),(203,'Uilis','FR',1970,'',52,7),(204,'Tom Crus','FR',1970,'',52,13),(205,'Rridli','FR',1970,'',52,2),(206,'Shwarceneger','FR',1970,'',52,13),(207,'Dueyn','FR',1970,'',41,14),(208,'Grant Keri','FR',1970,'',118,2),(209,'Garbo Greta','FR',1970,'',117,12),(210,'Grant Ki','FR',1970,'',44,9),(211,'Бенедикт Самюель','FR',1970,'',42,11),(212,'Eva Longoria','FR',1970,'',46,11),(214,'Merfi','FR',1970,'',53,6),(215,'Uilis','FR',1970,'',65,13),(216,'Zalman','FR',1970,'',52,3),(217,'Tom Crus','FR',1970,'',54,4),(218,'Rridli','USA',1970,'',14,11),(219,'Shwarceneger','USA',1970,'',20,14),(220,'Eva Longoria','FR',1970,'',52,5),(221,'Redclif','FR',1970,'',52,13),(222,'Merfi','FR',1970,'',52,8),(223,'Uilis','FR',1970,'',52,13),(224,'Tom Crus','FR',1970,'',52,13),(225,'Rridli','FR',1970,'',52,10),(226,'Shwarceneger','FR',1970,'',52,13),(227,'Dueyn','FR',1970,'',41,3),(228,'Grant Keri','FR',1970,'',118,8),(229,'Garbo Greta','FR',1970,'',117,1),(230,'Grant Ki','FR',1970,'',44,10),(231,'Бенедикт Самюель','FR',1970,'',42,4);
/*!40000 ALTER TABLE `actors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT 'people',
  `year` date NOT NULL DEFAULT '1970-01-01',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (1,'Френк Герберт','1970-01-01'),(2,'Михаил Булгаков','1970-01-01'),(3,'Ждек Лондон','1970-01-01'),(4,'Иоган Гёте','1970-01-01'),(5,'Роберт Хайнлайн','1970-01-01');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authors_books`
--

DROP TABLE IF EXISTS `authors_books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authors_books` (
  `author_id` int NOT NULL DEFAULT '0',
  `book_id` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors_books`
--

LOCK TABLES `authors_books` WRITE;
/*!40000 ALTER TABLE `authors_books` DISABLE KEYS */;
INSERT INTO `authors_books` VALUES (1,4),(2,1),(3,3),(4,2),(2,6);
/*!40000 ALTER TABLE `authors_books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `books` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT 'noname',
  `genre_id` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (1,'Мастер и Маргарита',2),(2,'Фауст',0),(3,'Белый клык',3),(4,'Дюна',1),(5,'Война и мир',2),(6,'Роковые яйца',2);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `director`
--

DROP TABLE IF EXISTS `director`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `director` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `country` varchar(50) NOT NULL DEFAULT '',
  `year` int NOT NULL DEFAULT '1970',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `director`
--

LOCK TABLES `director` WRITE;
/*!40000 ALTER TABLE `director` DISABLE KEYS */;
INSERT INTO `director` VALUES (1,'роберт Земекис','FR',1970),(2,'Роман Полански','FR',1970),(3,'Мартин Скорсезe','IT',1970),(4,'Джордж Лукас','USA',1970),(5,'Альфред Хичкок','USA',1970),(6,'Дэнни Вильнев','FR',1970),(7,'Дэвид Финчер','EN',1970),(8,'Квентин Тарантино','MEX',1970),(9,'Кристофер Нолан','USA',1970),(10,'Кристофер Ламберт','RU',1997),(11,'Джо Трахатовски','POL',1984);
/*!40000 ALTER TABLE `director` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `director_movi`
--

DROP TABLE IF EXISTS `director_movi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `director_movi` (
  `Id_director` int DEFAULT NULL,
  `id_movi` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `director_movi`
--

LOCK TABLES `director_movi` WRITE;
/*!40000 ALTER TABLE `director_movi` DISABLE KEYS */;
INSERT INTO `director_movi` VALUES (1,1),(2,2),(3,3),(4,4),(5,6),(6,8),(7,9),(8,3),(9,15),(11,16),(12,17),(1,18),(5,19),(7,6);
/*!40000 ALTER TABLE `director_movi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genres`
--

DROP TABLE IF EXISTS `genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `genres` (
  `id` int NOT NULL AUTO_INCREMENT,
  `genre` varchar(100) NOT NULL DEFAULT 'unknown',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genres`
--

LOCK TABLES `genres` WRITE;
/*!40000 ALTER TABLE `genres` DISABLE KEYS */;
INSERT INTO `genres` VALUES (1,'SF'),(2,'novel'),(3,'story'),(4,'horror');
/*!40000 ALTER TABLE `genres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movi`
--

DROP TABLE IF EXISTS `movi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `movietitle` varchar(100) NOT NULL DEFAULT '',
  `ear` int NOT NULL DEFAULT '2010',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movi`
--

LOCK TABLES `movi` WRITE;
/*!40000 ALTER TABLE `movi` DISABLE KEYS */;
INSERT INTO `movi` VALUES (1,'Noy',2010),(2,'independed day',1999),(3,'Эта замечательная жизнь',1946),(4,'Рассекая волны',1996),(5,'Форрест Гамп',1994),(6,'1+1',2012),(7,'Райя и последний дракон',2021),(8,'По соображениям совести',2004),(9,'Тайна Коко',2014),(10,'Небо измеряется милями',2019),(11,'Последний самурай',2004),(12,'Ford против Ferrari',2019);
/*!40000 ALTER TABLE `movi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Products`
--

DROP TABLE IF EXISTS `Products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Products` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(30) NOT NULL,
  `Manufacturer` varchar(20) NOT NULL,
  `ProductCount` int DEFAULT '0',
  `Price` decimal(10,0) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Products`
--

LOCK TABLES `Products` WRITE;
/*!40000 ALTER TABLE `Products` DISABLE KEYS */;
INSERT INTO `Products` VALUES (1,'iPhone X','Apple',3,79000),(4,'Galaxy S9','Samsung',5,59000),(5,'Galaxy S8','Samsung',4,49000),(7,'Nokia 8','HMD Global',6,41000),(8,'iPhone X','Apple',3,79000),(11,'Galaxy S9','Samsung',5,59000),(12,'Galaxy S8','Samsung',4,49000),(14,'Nokia 8','HMD Global',6,41000),(15,'iPhone X','Apple',3,79000),(18,'Galaxy S9','Samsung',5,59000),(19,'Galaxy S8','Samsung',4,49000),(21,'Nokia 8','HMD Global',6,41000);
/*!40000 ALTER TABLE `Products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` char(10) DEFAULT NULL,
  `pwd` char(15) DEFAULT NULL,
  `email` char(20) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Vasya','21341234qwfsdf','mmm@mmail.com','m'),(2,'Alex','21341234','mmm@gmail.com','m'),(3,'Alexey','qq21341234Q','alexey@gmail.com','m'),(4,'Helen','MarryMeeee','hell@gmail.com','f'),(5,'Jenny','SmakeMyb','eachup@gmail.com','f'),(6,'Lora','burn23','tpicks@gmail.com','f');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vocabulary`
--

DROP TABLE IF EXISTS `vocabulary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vocabulary` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `info` text,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vocabulary`
--

LOCK TABLES `vocabulary` WRITE;
/*!40000 ALTER TABLE `vocabulary` DISABLE KEYS */;
INSERT INTO `vocabulary` VALUES (1,'animals',NULL),(2,'school',NULL),(3,'nature',NULL),(4,'human',NULL),(5,'SF',NULL);
/*!40000 ALTER TABLE `vocabulary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `word`
--

DROP TABLE IF EXISTS `word`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `word` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(255) DEFAULT NULL,
  `vocabulary_id` int DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `word`
--

LOCK TABLES `word` WRITE;
/*!40000 ALTER TABLE `word` DISABLE KEYS */;
INSERT INTO `word` VALUES (1,'turtle',1),(2,'pig',1),(3,'dog',1),(4,'cat',1),(5,'lizard',1),(6,'cow',1),(7,'rabbit',1),(8,'frog',1),(9,'headgehog',1),(10,'goat',1),(11,'desk',2),(12,'book',2),(13,'chalk',2),(14,'pen',2),(15,'pencil',2),(16,'copybook',2),(17,'lesson',2),(18,'teacher',2),(19,'pupils',2),(20,'school',2),(21,'ray',3),(22,'thunder',3),(23,'sun',3),(24,'field',3),(25,'hill',3),(26,'mountain',3),(27,'river',3),(28,'forest',3),(29,'grass',3),(30,'rain',3),(31,'hair',4),(32,'nail',4),(33,'finger',4),(34,'eye',4),(35,'tooth',4),(36,'knee',4),(37,'elbow',4),(38,'leg',4),(39,'arm',4),(40,'head',4),(41,'engine',5),(42,'steel',5),(43,'power',5),(44,'nuclear',5),(45,'shotgun',5),(46,'laser',5),(47,'flight',5),(48,'energy',5),(49,'Moon',5),(50,'splace',5);
/*!40000 ALTER TABLE `word` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `words`
--

DROP TABLE IF EXISTS `words`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `words` (
  `id` int NOT NULL AUTO_INCREMENT,
  `word` varchar(100) NOT NULL DEFAULT '',
  `voc_id` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `words`
--

LOCK TABLES `words` WRITE;
/*!40000 ALTER TABLE `words` DISABLE KEYS */;
INSERT INTO `words` VALUES (1,'Earth',1),(2,'cat',2),(3,'dog',2),(4,'donkey',2),(30,'cat',2),(31,'dog',2),(32,'donkey',2),(33,'Earth',0),(34,'cat',0),(35,'dog',0),(36,'donkey',0),(37,'cat',0),(38,'dog',0),(39,'donkey',0),(40,'test',2);
/*!40000 ALTER TABLE `words` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-25 18:43:50
